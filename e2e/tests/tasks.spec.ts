import cypress from 'cypress'
import { uuid } from 'uuidv4'
import {ApiFunctions} from '../plugins/serverFunctions'

import {ListsTab,TasksTab,CalenderTab} from '../infrastructure/homePage.map';

import * as commands from '../support/commands';
import * as preData  from './tasks.predata';

const listsTab = new ListsTab;
const tasksTab = new TasksTab;
const calenderTab = new CalenderTab;
const apiFunctions = new ApiFunctions; // I know i should be aware for backend Changes

describe("tasks tests", () => {
    const newListName = uuid();

    beforeEach(() => {
        apiFunctions.deleteAllLists()
        cy.visit("/react-hooks-todolist");
        cy.get(listsTab.newListNameField)
            .type(newListName + "{enter}");
    });

    it("should add a new task", () => {
        const taskName = uuid();
        cy.get(tasksTab.newTaskNameField).type(taskName + '{enter}');
        cy.get(tasksTab.taskNameLine).contains(taskName)
    })

    it("should delete an existing task", () => {
        const taskName = uuid();
        cy.get(tasksTab.newTaskNameField).type(taskName + '{enter}');
        cy.get(tasksTab.deleteTaskButton).last().click();
        cy.get(tasksTab.taskTable)
            .should('not.contain', taskName);
    });

    it("should edit an existing task", () => {
        const taskName = uuid();
        const newTaskName = uuid();
        cy.get(tasksTab.newTaskNameField).type(taskName + '{enter}');
        //cy.get(selector.taskNameLine).invoke('hover');
        cy.get(tasksTab.editTaskButton).click();
        cy.get(tasksTab.editTaskNameField).type(newTaskName + '{enter}');

        cy.get(tasksTab.taskNameLine).contains(newTaskName);
    });

    it("after an uncompleted task is being clicked it should be marked as completed", () => {
        const taskName = uuid();
        cy.get(tasksTab.newTaskNameField).type(taskName + '{enter}');
        cy.get(tasksTab.taskNameLine).last().click();

        cy.get(tasksTab.taskNameStyle).last().should("have.css", "text-decoration", preData.completedTaskCssTextDecoration);
    });

    it("after a completed task is being clicked it should be marked as uncompleted", () => {
        const taskName = uuid();
        cy.get(tasksTab.newTaskNameField).type(taskName + '{enter}');
        cy.get(tasksTab.taskNameLine).last().click();
        cy.get(tasksTab.taskNameLine).last().click();
        cy.get(tasksTab.taskNameStyle).last().should("have.css", "text-decoration", preData.unCompletedTaskCssTextDecoration);
    });

    it("should edit the date of a task", () => {
        const taskName = uuid();
        cy.get(tasksTab.newTaskNameField).type(taskName + '{enter}');
        cy.get(tasksTab.editDateField).type('{selectall}').type(preData.newDate);

        cy.get(tasksTab.editDateField).should("have.value", preData.newDate);
    });

    it("should reset for defult date after removing finish date", () => {
        const taskName = uuid();
        
        cy.get(tasksTab.newTaskNameField).type(taskName + '{enter}');
        cy.get(tasksTab.editDateField).type('{selectall}').type(preData.newDate);
        cy.get(tasksTab.removeDateButton).click();

        cy.get(tasksTab.editDateField).should("have.value", preData.defultDate);
    });

})

describe("bonus tests", () => {
    const newListName = uuid();

    beforeEach(() => {
        apiFunctions.deleteAllLists()
        cy.visit("/react-hooks-todolist");
        cy.get(listsTab.newListNameField)
        .type(newListName + "{enter}");
    });

    it("should not crush if i delete the date", () => {
        const taskName = uuid();
        cy.get(tasksTab.newTaskNameField).type(taskName + '{enter}');
        cy.get(tasksTab.editDateField).type('{selectall}{backspace}')
    });

    it("should edit the date of a task with calender", () => {
        const taskName = uuid();
        const currentDate = '25/09/1997'
        const newDate = '15/06/1997'
        cy.get(tasksTab.newTaskNameField).type(taskName + '{enter}');
        cy.get(tasksTab.editDateField).type('{selectall}').type(currentDate);

        cy.get(tasksTab.calenderButton).first().click();
        commands.monthsBackward(3);
        cy.get(calenderTab.dayButton).contains(15).click();
        cy.get(tasksTab.editDateField).first().should("have.value", newDate);

    });

})



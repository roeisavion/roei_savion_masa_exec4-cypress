import cypress from 'cypress'
import {uuid} from 'uuidv4'
import {ApiFunctions} from '../plugins/serverFunctions'

import {ListsTab} from '../infrastructure/homePage.map';

const listsTab = new ListsTab;
const apiFunctions = new ApiFunctions; // I know i should be aware for backend Changes

describe("todo-list tests", () => {
    beforeEach(() =>{
        apiFunctions.deleteAllLists()
        cy.visit("/react-hooks-todolist");
    })

    it("add a new list", () => {
        const newListName = uuid(); 
        cy.get(listsTab.newListNameField)
        .type(newListName +"{enter}")  

        cy.get(listsTab.showTodoLists).click()
        cy.get(listsTab.listsNamesSelector).contains(newListName)  
    })


    it("cencels deletion of a list", () =>{
        const newListName = uuid(); 
        cy.get(listsTab.newListNameField)
        .type( newListName + "{enter}");

        cy.get(listsTab.showTodoLists).click()
        cy.get(listsTab.deleteListButton(newListName)).click();
        cy.get(listsTab.deleteListNoButton).click();
        
        cy.get(listsTab.listsNamesSelector).contains(newListName);
    })

    it("delete a list", () =>{
        const newListName = uuid(); 
        cy.get(listsTab.newListNameField)
        .type( newListName + "{enter}");

        cy.get(listsTab.showTodoLists).click()
        cy.get(listsTab.deleteListButton(newListName)).click();

        cy.get(listsTab.deleteListYesButton).click();
        
        cy.get(listsTab.listsNamesSelector)
        .should('not.contain',newListName)
    })

})



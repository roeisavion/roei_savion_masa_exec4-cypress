export class ListsTab {
    newListNameField = '[placeholder="Add your task list here..."]';
    showTodoLists = '[data-test=openButton]' ;
    listsNamesSelector = '[data-test=MainContainer]' ;
    deleteListButton = ((listName:string) => {
        return `[data-test="removeIcon ${listName}"]`
    });
    deleteListNoButton = '[data-test=noBut]' ;
    deleteListYesButton = '[data-test=yesBut]' ;
}
export class TasksTab {
    newTaskNameField = '[placeholder="Add your task here..."]';
    editDateField = '#date-picker-inline' ;
    removeDateButton = '[data-test=removeDateButton]';
    taskNameLine = '[data-test=TodoLine]'
    deleteTaskButton = '[data-test=removeIcon]' ;
    editTaskButton = '[data-test=editIcon]' ;
    editTaskNameField = '[data-test=TodoContainer]>li>form>input[data-test=input]' ;
    calenderButton = '[aria-label="change date"]' ;
    taskTable = '[data-test=TodoContainer]' ;
    taskNameStyle = '[data-test=TodoLine]>span' ;
}

export class CalenderTab {
    monthAndYearInCalender = 'div>p.MuiTypography-root';
    moveMonth = 'div.MuiPickersCalendarHeader-switchHeader>button';
    dayButton = 'p.MuiTypography-root';
}









import axios from 'axios'

import {TodoList } from './types';


export class ApiFunctions {

    async getAllTodoLists(): Promise<TodoList[]> {
        return (await axios.get('http://localhost:4000/todo-list')).data;
    }

    async deleteTodoList(listId: string) {
        return await axios.delete(`http://localhost:4000/todo-list/${listId}`);
    }

    async deleteAllLists(): Promise<void> {
        const allListsArray = await this.getAllTodoLists();
        allListsArray.forEach((list) => {
            this.deleteTodoList(list.id)
        })
    }
}


